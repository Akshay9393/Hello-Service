package com.nuance.org;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@SpringBootApplication
public class HelloServiceApplication {

	  private static Logger log = LoggerFactory.getLogger(HelloServiceApplication.class);

	  @Autowired
	    Environment environment;
	  
	  @RequestMapping(value = "/greeting")
	  public String greet() {
	   log.info("Access /greeting");

	   List<String> greetings = Arrays.asList("Hi there", "Greetings", "Salutations");
	   Random rand = new Random();

	   int randomNum = rand.nextInt(greetings.size());
	   return greetings.get(randomNum);
	  }

	  @RequestMapping(value = "/")
	  public String home() {
	   log.info("Access /");
	   return "Hi!";
	  }
	  @GetMapping("/backend")
	    public String backend() {
	        System.out.println("Inside MyRestController::backend...");
	 
	        String serverPort = environment.getProperty("local.server.port");
	 
	        System.out.println("Port : " + serverPort);
	 
	        return "Hello form Backend!!! " + " Host : localhost " + " :: Port : " + serverPort;
	    }
	public static void main(String[] args) {
		SpringApplication.run(HelloServiceApplication.class, args);
	}

}
